<?php
// Start session
session_start();
if (empty($_SESSION['admin'])) {
	header('location:../index.php');
	exit();
}
require_once '../config/db.php';
require_once '../config/functions.php';
// Retrieve session data

$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';

// Get user data
$userData = array();
if(!empty($_GET['id'])){
    // Include and initialize DB class

	$db = new DB();

    // Fetch the user data
	$conditions['where'] = array(
		'id' => $_GET['id'],

	);
	$conditions['return_type'] = 'single';
	$userData = $db->getRows('cs', $conditions);
}
$userData = !empty($sessData['userData'])?$sessData['userData']:$userData;
unset($_SESSION['sessData']['userData']);

$actionLabel = !empty($_GET['id'])?'Edit':'Add';

// Get status message from session
if(!empty($sessData['status']['msg'])){
	$statusMsg = $sessData['status']['msg'];
	$statusMsgType = $sessData['status']['type'];
	unset($_SESSION['sessData']['status']);
}


$title = 'Projects > '.$actionLabel . ' Data';

?>

<?php require_once '../config/header.php'; ?>



<form method="post" action="userAction.php" enctype="multipart/form-data">
	<input type="hidden" name="section" value="Projects">
	<input type="hidden" name="id" value="<?php echo !empty($userData['id'])?$userData['id']:''; ?>">

	<div class="form-group">
		<label class="bmd-label-floating">Title</label>
		<input type="text" class="form-control" name="title" value="<?php echo !empty($userData['title'])?$userData['title']:''; ?>" required>
	</div>

	<div class="form-group">
		<label class="bmd-label-floating">link</label>
		<input type="text" class="form-control" name="link" value="<?php echo !empty($userData['link'])?$userData['link']:''; ?>" required>
	</div>

	<div class="form-group">
		<label for="exampleFormControlTextarea1" class="bmd-label-floating">Description</label>
		<textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="3" required>
			<?php echo !empty($userData['description'])?$userData['description']:''; ?>
		</textarea>
	</div>

	<div class="form-group">
		<label for="exampleFormControlFile1"><i class="material-icons">photo_camera</i>Upload Image</label>

		<input type="file" name="image" class="btn btn-danger" id="exampleFormControlFile1">
	</div>

	<input type="submit" name="userSubmit" class="btn btn-info" value="Submit">
</form>
</div>

<?php require_once '../config/footer.php'; ?>