<?php
// Start session
session_start();

// Include and initialize DB class
require_once '../config/db.php';
require_once '../config/functions.php';
$db = new DB();

// Database table name
$tblName = 'cs';

// Set default redirect url
$redirectURL = 'index.php';

if(isset($_POST['userSubmit'])){
    // Get form fields value
    $section     = trim(strip_tags($_POST['section']));
    $address     = trim(strip_tags($_POST['address']));
    $facebook     = trim(strip_tags($_POST['facebook']));
    $linkedIn     = trim(strip_tags($_POST['linkedIn']));
    $freeLancer     = trim(strip_tags($_POST['freeLancer']));
    $email    = trim(strip_tags($_POST['email']));
    $no1    = trim(strip_tags($_POST['no1']));
    $no2    = trim(strip_tags($_POST['no2']));
    
    // Fields validation
    $errorMsg = '';
    if(empty($address) || !filter_var($address, FILTER_SANITIZE_STRING)){
        $errorMsg .= '<p>Please enter your address.</p>';
    }
    if(empty($facebook) || !filter_var($facebook, FILTER_SANITIZE_URL)){
        $errorMsg .= '<p>Please enter your facebook.</p>';
    }
    if(empty($linkedIn) || !filter_var($linkedIn, FILTER_SANITIZE_URL)){
        $errorMsg .= '<p>Please enter your linkedIn.</p>';
    }
    if(empty($freeLancer) || !filter_var($freeLancer, FILTER_SANITIZE_URL)){
        $errorMsg .= '<p>Please enter your freeLancer.</p>';
    }
    if(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)){
        $errorMsg .= '<p>Please enter a valid email.</p>';
        //Other Condition
    }
    if(empty($no1) || !preg_match("/^[-+0-9]{6,20}$/", $no1)){
        $errorMsg .= '<p>Please enter a valid phone number.</p>';
    }
     if(empty($no2) || !preg_match("/^[-+0-9]{6,20}$/", $no2)){
        $errorMsg .= '<p>Please enter a valid phone number.</p>';
    }
    
    // Submitted form data
    $userData = array(
        'section' => $section,
        'description' => $address,
        'no1' => $no1,
        'facebook' => $facebook,
        'linkedIn' => $linkedIn,
        'freeLancer' => $freeLancer,
        'title' => $email,
        'no2' => $no2
    );
    
    // Store the submitted field value in the session
    $sessData['userData'] = $userData;
    
    // Submit the form data
    if(empty($errorMsg)){
        if(!empty($_POST['id'])){
            // Update user data
            $condition = array('id' => $_POST['id']);
            $update = $db->update($tblName, $userData, $condition);
            
            if($update){
                $sessData['status']['type'] = 'success';
                $sessData['status']['msg'] = 'data has been updated successfully.';
                
                // Remote submitted fields value from session
                unset($sessData['userData']);
            }else{
                $sessData['status']['type'] = 'error';
                $sessData['status']['msg'] = 'No Data Changed If you want to exit click Back btn';
                
                // Set redirect url
                $redirectURL = 'addEdit.php?id='.$_POST['id'];
            }
        }else{
            // Insert user data
            $insert = $db->insert($tblName, $userData);
            
            if($insert){
                $sessData['status']['type'] = 'success';
                $sessData['status']['msg'] = 'data has been added successfully.';
                
                // Remote submitted fields value from session
                unset($sessData['userData']);
            }else{
                $sessData['status']['type'] = 'error';
                $sessData['status']['msg'] = 'cant insert, Some problem occurred, please try again.';
                
                // Set redirect url
                $redirectURL = 'addEdit.php';
            }
        }
    }else{
        $sessData['status']['type'] = 'error';
        $sessData['status']['msg'] = '<p>Please fill all the mandatory fields.</p>'.$errorMsg;
        
        // Set redirect url
        $redirectURL = 'addEdit.php';
    }
    
    // Store status into the session
    $_SESSION['sessData'] = $sessData;
}elseif(($_REQUEST['action_type'] == 'delete') && !empty($_GET['id'])){
    // Delete data
    $condition = array('id' => $_GET['id']);
    $delete = $db->delete($tblName, $condition);
    
    if($delete){
        $sessData['status']['type'] = 'success';
        $sessData['status']['msg'] = 'data has been deleted successfully.';
    }else{
        $sessData['status']['type'] = 'error';
        $sessData['status']['msg'] = 'Some problem occurred, please try again.';
    }
    
    // Store status into the session
    $_SESSION['sessData'] = $sessData;
}

// Redirect to the respective page
header("Location:".$redirectURL);
exit();
?>