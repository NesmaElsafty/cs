<?php 

session_start();
if (empty($_SESSION['admin'])) {
	header('location:../index.php');
	exit();
}

require_once '../config/db.php';
require_once '../config/functions.php';

// Retrieve session data
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';

// Include and initialize DB class

$db = new DB();

// Fetch the users data
$conditions['return_type'] = 'single';
$conditions['where'] = array(
	'section' => 'ContactUS'
);
$data = $db->getRows('cs', $conditions);

// Get status message from session
if(!empty($sessData['status']['msg'])){
	$statusMsg = $sessData['status']['msg'];
	$statusMsgType = $sessData['status']['type'];
	unset($_SESSION['sessData']['status']);
}


$title = 'ContactUS';

?>

<?php require_once '../config/header.php'; ?>

		<div class="card" style="width: 50%; margin-top: -25px;">
		  
		  <div class="card-body">
		    <h5 class="card-title"><strong> Email: </strong></h5>
		    <p class="card-text"><?php echo $data['title']; ?></p>

		    <h5 class="card-title"><strong> facebook: </strong></h5>
		    <p class="card-text"><?php echo $data['facebook']; ?></p>

		    <h5 class="card-title"><strong> linkedIn: </strong></h5>
		    <p class="card-text"><?php echo $data['linkedIn']; ?></p>

		    <h5 class="card-title"><strong> freeLancer: </strong></h5>
		    <p class="card-text"><?php echo $data['freeLancer']; ?></p>

		    <h5 class="card-title"><strong> Address: </strong></h5>
		    <p class="card-text"><?php echo $data['description']; ?></p>

		    <h5 class="card-title"><strong> Phone 1: </strong></h5>
		    <p class="card-text"><?php echo $data['no1']; ?></p>

		    <h5 class="card-title"><strong> Phone 2: </strong></h5>
		    <p class="card-text"><?php echo $data['no2']; ?></p>
		    <div style="float: right;">
		    <?php if(empty($data)) { ?>
		    	<a href="addEdit.php" ><i title="add" class="material-icons">add_circle_outline</i></a>
		    <?php }else{ ?>
		    	<a href="addEdit.php?id=<?php echo $data['id']; ?>" ><i title="Update" class="material-icons">settings_applications</i></a>
		    <?php } ?>
		    <a href="userAction.php?action_type=delete&id=<?php echo $data['id']; ?>"  onclick="return confirm('Are you sure to delete?');"><i title="delete" class="material-icons">delete_forever</i></a>
		  	</div>
		  </div>
		</div>
<div style="margin:110px;"></div>						
<?php require_once '../config/footer.php'; ?>
