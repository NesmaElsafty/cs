<?php
// Start session
session_start();
if (empty($_SESSION['admin'])) {
	header('location:../index.php');
	exit();
}
require_once '../config/db.php';
require_once '../config/functions.php';
// Retrieve session data

$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';

// Get user data
$userData = array();
if(!empty($_GET['id'])){
    // Include and initialize DB class

	$db = new DB();

    // Fetch the user data
	$conditions['where'] = array(
		'id' => $_GET['id'],
	);
	$conditions['return_type'] = 'single';
	$data = $db->getRows('cs', $conditions);
}
$userData = !empty($sessData['userData'])?$sessData['userData']:$userData;
unset($_SESSION['sessData']['userData']);

$actionLabel = !empty($_GET['id'])?'Edit':'Add';

// Get status message from session
if(!empty($sessData['status']['msg'])){
	$statusMsg = $sessData['status']['msg'];
	$statusMsgType = $sessData['status']['type'];
	unset($_SESSION['sessData']['status']);
}
$title = 'ContactUS > '.$actionLabel . ' Data';

?>

<?php require_once '../config/header.php'; ?>

	<!-- Display status message -->
	<?php if(!empty($statusMsg) && ($statusMsgType == 'success')){ ?>
		<div class="col-xs-12">
			<div class="alert alert-success"><?php echo $statusMsg; ?></div>
		</div>
	<?php }elseif(!empty($statusMsg) && ($statusMsgType == 'error')){ ?>
		<div class="col-xs-12">
			<div class="alert alert-danger"><?php echo $statusMsg; ?></div>
		</div>
	<?php } ?>


	<form method="post" action="userAction.php">
		<input type="hidden" name="section" value="ContactUS">
		<input type="hidden" name="id" value="<?php echo !empty($data['id'])?$data['id']:''; ?>">

		<div class="col-md-4">
			<div class="form-group">
				<label class="bmd-label-floating">Email address</label>
				<input type="email" class="form-control" name="email" value="<?php echo !empty($data['title'])?$data['title']:''; ?>" required>
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
				<label class="bmd-label-floating">Address</label>
				<input type="text" class="form-control" name="address" value="<?php echo !empty($data['description'])?$data['description']:''; ?>" required>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="bmd-label-floating">Facebook</label>
				<input type="text" class="form-control" name="facebook" value="<?php echo !empty($data['facebook'])?$data['facebook']:''; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="bmd-label-floating">linkedIn</label>
				<input type="text" class="form-control" name="linkedIn" value="<?php echo !empty($data['linkedIn'])?$data['linkedIn']:''; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="bmd-label-floating">freeLancer</label>
				<input type="text" class="form-control" name="freeLancer" value="<?php echo !empty($data['freeLancer'])?$data['freeLancer']:''; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="bmd-label-floating">Phone 1</label>
				<input type="number" class="form-control" name="no1"  value="<?php echo !empty($data['no1'])?$data['no1']:''; ?>" required>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label class="bmd-label-floating">Phone 2</label>
				<input type="number" class="form-control"  name="no2"  value="<?php echo !empty($data['no2'])?$data['no2']:''; ?>">
			</div>
		</div>

		<input type="submit" name="userSubmit" class="btn btn-primary pull-left" value="Submit">
	</form>
	

	<?php require_once '../config/footer.php'; ?>
