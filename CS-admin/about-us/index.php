<?php 

session_start();
if (empty($_SESSION['admin'])) {
	header('location:../index.php');
	exit();
}

require_once '../config/db.php';
require_once '../config/functions.php';

// Retrieve session data
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';

// Include and initialize DB class

$db = new DB();

// Fetch the users data
$conditions['return_type'] = 'single';
$conditions['where'] = array(
	'section' => 'AboutUs'
);
$data = $db->getRows('cs', $conditions);

// Get status message from session
if(!empty($sessData['status']['msg'])){
	$statusMsg = $sessData['status']['msg'];
	$statusMsgType = $sessData['status']['type'];
	unset($_SESSION['sessData']['status']);
}
$title = 'AboutUs';

?>

<?php require_once '../config/header.php'; ?>

<!-- Display status message -->
<?php if(!empty($statusMsg) && ($statusMsgType == 'success')){ ?>
	<div class="col-xs-12">
		<div class="alert alert-success"><?php echo $statusMsg; ?></div>
	</div>
<?php }elseif(!empty($statusMsg) && ($statusMsgType == 'error')){ ?>
	<div class="col-xs-12">
		<div class="alert alert-danger"><?php echo $statusMsg; ?></div>
	</div>
<?php } ?>

<div class="card mb-3" style="width: 75%; text-align: center; margin-left: 100px; margin-top: -20px;">
  <img class="card-img-top" src="images/<?php echo $data['image']; ?>" alt="Card image cap">
  <div class="card-body">
    
    <p class="card-text"><?php echo $data['description']; ?></p>
    <?php if(empty($data)) { ?>
			
			<a href="addEdit.php" class="btn btn-success"><i class="plus"></i> New Data</a>
			
		<?php }else{ ?>
			
			<a href="addEdit.php?id=<?php echo $data['id']; ?>" class="btn btn-primary"><i class="plus"></i> Update Data</a>
			
		<?php } ?>
		<a href="userAction.php?action_type=delete&id=<?php echo $data['id']; ?>" class="btn btn-danger" onclick="return confirm('Are you sure to delete?');">delete</a>
  </div>
</div>




<?php require_once '../config/footer.php'; ?>
