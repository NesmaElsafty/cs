<?php
// Start session
session_start();
if (empty($_SESSION['admin'])) {
	header('location:../index.php');
	exit();
}
require_once '../config/db.php';
require_once '../config/functions.php';
// Retrieve session data

$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';

// Get user data
$userData = array();
if(!empty($_GET['id'])){
    // Include and initialize DB class

	$db = new DB();

    // Fetch the user data
	$conditions['where'] = array(
		'id' => $_GET['id'],
		'section' => 'AboutUs'
	);
	$conditions['return_type'] = 'single';
	$userData = $db->getRows('cs', $conditions);
}
$userData = !empty($sessData['userData'])?$sessData['userData']:$userData;
unset($_SESSION['sessData']['userData']);

$actionLabel = !empty($_GET['id'])?'Edit':'Add';

// Get status message from session
if(!empty($sessData['status']['msg'])){
	$statusMsg = $sessData['status']['msg'];
	$statusMsgType = $sessData['status']['type'];
	unset($_SESSION['sessData']['status']);
}
$title = 'AboutUs > '.$actionLabel . ' Data';

?>

<?php require_once '../config/header.php'; ?>

	
	<!-- Display status message -->
	<?php if(!empty($statusMsg) && ($statusMsgType == 'success')){ ?>
		<div class="col-xs-12">
			<div class="alert alert-success"><?php echo $statusMsg; ?></div>
		</div>
	<?php }elseif(!empty($statusMsg) && ($statusMsgType == 'error')){ ?>
		<div class="col-xs-12">
			<div class="alert alert-danger"><?php echo $statusMsg; ?></div>
		</div>
	<?php } ?>

			<form method="post" action="userAction.php" enctype="multipart/form-data">
				<input type="hidden" name="section" value="AboutUs">
				<input type="hidden" name="id" value="<?php echo !empty($userData['id'])?$userData['id']:''; ?>">

				<div class="form-group">
				    <label for="exampleFormControlTextarea1">Description</label>
				    <textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="3" required>
				    	<?php echo !empty($userData['description'])?$userData['description']:''; ?>
				    </textarea>
				  </div>
				  
				  <div class="form-group">
				    <label for="exampleFormControlFile1"><i class="material-icons">photo_camera</i>Upload Image</label>
				    
				    <input type="file" name="image" class="btn btn-danger" id="exampleFormControlFile1">
				  </div>
			

				<input type="submit" name="userSubmit" class="btn btn-info" value="Submit">
			</form>




<?php require_once '../config/footer.php'; ?>
