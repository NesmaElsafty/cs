<?php 

session_start();


if (empty($_SESSION['admin'])) {
    header('location:../index.php');
    exit();
}

require_once '../config/db.php';
require_once '../config/functions.php';

// Retrieve session data
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';

// Include and initialize DB class

$db = new DB();

// Fetch the users data

$conditions['where'] = array(
    'section' => 'Services'
);
$data = $db->getRows('cs', $conditions);

// Get status message from session
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusMsgType = $sessData['status']['type'];
    unset($_SESSION['sessData']['status']);
}


$title = 'Services';

?>

<?php require_once '../config/header.php'; ?>

<a href="addEdit.php">
    <button class="btn btn-primary">Add Service</button>
</a>
<div class="card-group">
    <?php if(!empty($data)){ $count = 0; foreach($data as $row){ ?>

        <div class="card" style="width: 25%; margin:20px; padding: 10px; border:10px; height: 20%;">
            <div class="card-body">
              <h5 class="card-title"><strong><?php echo $row['title']; ?></strong></h5>
              <p class="card-text"><?php echo $row['description']; ?></p>
          </div>
          <div class="card-footer">
              <a href="addEdit.php?id=<?php echo $row['id']; ?>" >
                <i title="Update" class="material-icons">settings_applications</i>
            </a>

            <a href="userAction.php?action_type=delete&id=<?php echo $row['id']; ?>"  onclick="return confirm('Are you sure to delete?');">
                <i title="delete" class="material-icons">delete_forever</i>
            </a>
        </div>
    </div>
<?php } } ?>
</div>

<?php  require_once '../config/footer.php'; ?>
    