<?php
// Start session
// session_start();

// Include and initialize DB class
require_once '../config/db.php';
require_once '../config/functions.php';
$db = new DB();

// Database table name
$tblName = 'cs';

// Set default redirect url
$redirectURL = 'index.php';

if(isset($_POST['userSubmit'])){
    // Get form fields value
    $description     = trim(strip_tags($_POST['description']));
    $title     = trim(strip_tags($_POST['title']));
    $section     = trim(strip_tags($_POST['section']));

    $image = $_FILES['image']['name'];
    $tmp = $_FILES["image"]["tmp_name"];
    
    // Fields validation
    $errorMsg = '';
    if(empty($title)  || !filter_var($title, FILTER_SANITIZE_STRING)){
        $errorMsg .= '<p>Please enter your title.</p>';
    }
    if(empty($description)  || !filter_var($description, FILTER_SANITIZE_STRING)){
        $errorMsg .= '<p>Please enter your description.</p>';
    }
    // Submitted form data
    if (empty($image)) {

       $userData = array(
        'description' => $description,
        'title' => $title,
        'section' => $section,
        'image' => OldImg($tblName,$_POST['id'])
    );
   }else{
    $upload = uploadImage($image, $tmp);
    $userData = array(
        'title' => $title,
        'description' => $description,
        'section' => $section,
        'image' => $image
    );
}

    // Store the submitted field value in the session
$sessData['userData'] = $userData;

    // Submit the form data
if(empty($errorMsg)){
    if(!empty($_POST['id'])){
            // Update user data
        $condition = array('id' => $_POST['id']);
        $update = $db->update($tblName, $userData, $condition);

        if($update){
            $sessData['status']['type'] = 'success';
            $sessData['status']['msg'] = 'data has been updated successfully.';

                // Remote submitted fields value from session
            unset($sessData['userData']);
        }else{
            $sessData['status']['type'] = 'error';
            $sessData['status']['msg'] = 'Cant Update, Some problem occurred, please try again.';

                // Set redirect url
            $redirectURL = 'index.php';
        }
    }else{
            // Insert user data
        $insert = $db->insert($tblName, $userData);

        if($insert){
            $sessData['status']['type'] = 'success';
            $sessData['status']['msg'] = 'data has been added successfully.';

                // Remote submitted fields value from session
            unset($sessData['userData']);
        }else{
            $sessData['status']['type'] = 'error';
            $sessData['status']['msg'] = 'Cant Add,Some problem occurred, please try again.';

                // Set redirect url
            $redirectURL = 'addEdit.php';
        }
    }
}else{
    $sessData['status']['type'] = 'error';
    $sessData['status']['msg'] = '<p>Please fill all the mandatory fields.</p>'.$errorMsg;

        // Set redirect url
    $redirectURL = 'addEdit.php';
}

    // Store status into the session
$_SESSION['sessData'] = $sessData;
}elseif(($_REQUEST['action_type'] == 'delete') && !empty($_GET['id'])){
    // Delete data
    $condition = array('id' => $_GET['id']);
    $delete = $db->delete($tblName, $condition);
    
    if($delete){
        $sessData['status']['type'] = 'success';
        $sessData['status']['msg'] = 'data has been deleted successfully.';
    }else{
        $sessData['status']['type'] = 'error';
        $sessData['status']['msg'] = 'Some problem occurred, please try again.';
    }
    
    // Store status into the session
    $_SESSION['sessData'] = $sessData;
}

// Redirect to the respective page
header("Location:".$redirectURL);
exit();
?>