<?php
$title = '';

session_start();

if (isset($_SESSION['admin'])) {
	header('location:home.php');
	exit();
}


$username = 'admin';
$password = '123456';

if (isset($_POST['login'])) {
	
	filter_var($_POST['name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	filter_var($_POST['password'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

	if ($_POST['name'] == $username && $_POST['password'] == $password) {
		

		$_SESSION['admin'] = $username;
		header('location:home.php');
	}else{
		$_SESSION['error'] = 'Please Type the correct username or password';
		// unset($_SESSION['error']);
	}
}
	?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login To CS</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="config/login/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="config/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="config/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="config/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="config/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="config/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="config/login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="config/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="config/login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="config/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="config/login/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(config/login/images/bg-01.jpg);">
					<span class="login100-form-title-1">
						Sign In
					</span>
				</div>
				<?php if (isset($_SESSION['error'])) { ?>
					<div class="alert alert-success" style="text-align: center;"><?php echo $_SESSION['error']; ?></div>
				<?php unset($_SESSION['error']); } ?>

				<form class="login100-form validate-form"  action='<?php echo $_SERVER["PHP_SELF"];?>' method='post'>
					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100">Username</span>
						<input class="input100" type="text" name="name" placeholder="Enter username">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="password" placeholder="Enter password">
						<span class="focus-input100"></span>
					</div>

					

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" name="login">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->
	<script src="config/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="config/login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="config/login/vendor/bootstrap/js/popper.js"></script>
	<script src="config/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="config/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="config/login/vendor/daterangepicker/moment.min.js"></script>
	<script src="config/login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="config/login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="config/login/js/main.js"></script>

</body>
</html>
