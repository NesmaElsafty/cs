<?php
// Start session
session_start();

// Include and initialize DB class
require_once '../config/db.php';
require_once '../config/functions.php';
$db = new DB();

// Database table name
$tblName = 'cs';

// Set default redirect url
$redirectURL = 'index.php';

if(isset($_POST['userSubmit'])){
    // Get form fields value
    $section     = trim(strip_tags($_POST['section']));
    $skill     = trim(strip_tags($_POST['skill']));

    $pre    = trim(strip_tags($_POST['pre']));

    
    // Fields validation
    $errorMsg = '';
    if(empty($skill) || !filter_var($skill, FILTER_SANITIZE_STRING)){
        $errorMsg .= '<p>Please enter your skill.</p>';
    }
    if(empty($pre) || !filter_var($pre, FILTER_SANITIZE_NUMBER_INT)){
        $errorMsg .= '<p>Please enter your pre.</p>';
    }
    
    
    // Submitted form data
    $userData = array(
        'section' => $section,
        'title' => $skill,
        'no1' => $pre,
    );
    
    // Store the submitted field value in the session
    $sessData['userData'] = $userData;
    
    // Submit the form data
    if(empty($errorMsg)){
        if(!empty($_POST['id'])){
            // Update user data
            $condition = array('id' => $_POST['id']);
            $update = $db->update($tblName, $userData, $condition);
            
            if($update){
                $sessData['status']['type'] = 'success';
                $sessData['status']['msg'] = 'data has been updated successfully.';
                
                // Remote submitted fields value from session
                unset($sessData['userData']);
            }else{
                $sessData['status']['type'] = 'error';
                $sessData['status']['msg'] = 'No Data Changed If you want to exit click Back btn';
                
                // Set redirect url
                $redirectURL = 'addEdit.php?id='.$_POST['id'];
            }
        }else{
            // Insert user data
            $insert = $db->insert($tblName, $userData);
            
            if($insert){
                $sessData['status']['type'] = 'success';
                $sessData['status']['msg'] = 'data has been added successfully.';
                
                // Remote submitted fields value from session
                unset($sessData['userData']);
            }else{
                $sessData['status']['type'] = 'error';
                $sessData['status']['msg'] = 'cant insert, Some problem occurred, please try again.';
                
                // Set redirect url
                $redirectURL = 'addEdit.php';
            }
        }
    }else{
        $sessData['status']['type'] = 'error';
        $sessData['status']['msg'] = '<p>Please fill all the mandatory fields.</p>'.$errorMsg;
        
        // Set redirect url
        $redirectURL = 'addEdit.php';
    }
    
    // Store status into the session
    $_SESSION['sessData'] = $sessData;
}elseif(($_REQUEST['action_type'] == 'delete') && !empty($_GET['id'])){
    // Delete data
    $condition = array('id' => $_GET['id']);
    $delete = $db->delete($tblName, $condition);
    
    if($delete){
        $sessData['status']['type'] = 'success';
        $sessData['status']['msg'] = 'data has been deleted successfully.';
    }else{
        $sessData['status']['type'] = 'error';
        $sessData['status']['msg'] = 'Some problem occurred, please try again.';
    }
    
    // Store status into the session
    $_SESSION['sessData'] = $sessData;
}

// Redirect to the respective page
header("Location:".$redirectURL);
exit();
?>