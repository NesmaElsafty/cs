<?php 

session_start();
if (empty($_SESSION['admin'])) {
	header('location:../index.php');
	exit();
}


require_once '../config/db.php';
require_once '../config/functions.php';

// Retrieve session data
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';

// Include and initialize DB class

$db = new DB();

// Fetch the users data

$conditions['where'] = array(
	'section' => 'skills'
);
$data = $db->getRows('cs', $conditions);

// Get status message from session
if(!empty($sessData['status']['msg'])){
	$statusMsg = $sessData['status']['msg'];
	$statusMsgType = $sessData['status']['type'];
	unset($_SESSION['sessData']['status']);
}


$title = 'Skills';

?>

<?php require_once '../config/header.php'; ?>
<a href="addEdit.php">
	<button class="btn btn-primary" style="margin-bottom: 50px; margin-top: -25px;">Add Skill</button>
</a>
<div class="card" style="width: 70%; margin-top: -25px;">

	<div class="card-body">
		<?php if (!empty($data)){  $count = 0; foreach($data as $row) { ?>
			<div class="row">
				<h5 class="card-title" style="margin-right: 10px; font-weight: bold;">  Skill Title:</h5>
				<h5 class="card-text" style="margin-right: 50%; color: purple; font-weight:bold;" ><?php echo $row['title']; ?></h5>

				<h5 class="card-title" style="margin-right: 10px; font-weight: bold;"><strong> Presentage: </strong></h5>
				<h5 class="card-text" style=" color: purple; font-weight:bold;"><?php echo $row['no1']; ?>%</h5>
				
				<a href="addEdit.php?id=<?php echo $row['id']; ?>" >
					<i title="Update" class="material-icons">settings_applications</i>
				</a>

				<a href="userAction.php?action_type=delete&id=<?php echo $row['id']; ?>"  onclick="return confirm('Are you sure to delete?');">
					<i title="delete" class="material-icons">delete_forever</i>
				</a>
			
			</div>
		<?php } } ?>

	</div>
</div>
<div style="margin:110px;"></div>						
<?php require_once '../config/footer.php'; ?>
