<?php
// Start session
session_start();
if (empty($_SESSION['admin'])) {
	header('location:../index.php');
	exit();
}
require_once '../config/db.php';
require_once '../config/functions.php';
// Retrieve session data

$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';

// Get user data
$userData = array();
if(!empty($_GET['id'])){
    // Include and initialize DB class

	$db = new DB();

    // Fetch the user data
	$conditions['where'] = array(
		'id' => $_GET['id'],
	);
	$conditions['return_type'] = 'single';
	$data = $db->getRows('cs', $conditions);
}
$userData = !empty($sessData['userData'])?$sessData['userData']:$userData;
unset($_SESSION['sessData']['userData']);

$actionLabel = !empty($_GET['id'])?'Edit':'Add';

// Get status message from session
if(!empty($sessData['status']['msg'])){
	$statusMsg = $sessData['status']['msg'];
	$statusMsgType = $sessData['status']['type'];
	unset($_SESSION['sessData']['status']);
}
$title = 'ContactUS > '.$actionLabel . ' Data';

?>

<?php require_once '../config/header.php'; ?>

	<!-- Display status message -->
	<?php if(!empty($statusMsg) && ($statusMsgType == 'success')){ ?>
		<div class="col-xs-12">
			<div class="alert alert-success"><?php echo $statusMsg; ?></div>
		</div>
	<?php }elseif(!empty($statusMsg) && ($statusMsgType == 'error')){ ?>
		<div class="col-xs-12">
			<div class="alert alert-danger"><?php echo $statusMsg; ?></div>
		</div>
	<?php } ?>


	<form method="post" action="userAction.php">
		<input type="hidden" name="section" value="Skills">
		<input type="hidden" name="id" value="<?php echo !empty($data['id'])?$data['id']:''; ?>">

		<div class="col-md-4">
			<div class="form-group">
				<label class="bmd-label-floating">Skill Title</label>
				<input type="text" class="form-control" name="skill" value="<?php echo !empty($data['title'])?$data['title']:''; ?>" required>
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
				<label class="bmd-label-floating">Presentage</label>
				<input type="number" class="form-control" name="pre"  value="<?php echo !empty($data['no1'])?$data['no1']:''; ?>" required>
			</div>
		</div>
		

		<input type="submit" name="userSubmit" class="btn btn-primary pull-left" value="Submit">
	</form>
	

	<?php require_once '../config/footer.php'; ?>
