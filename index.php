<?php 
require_once 'CS-admin/config/connect.php'; 

?>

<!DOCTYPE html>
<html lang="en">

  <head>
    <title>Coding Squad</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Muli:400,700" rel="stylesheet">

    <link rel="stylesheet" href="ui/fonts/icomoon/style.css">

    <link rel="stylesheet" href="ui/css/bootstrap.min.css">
    <link rel="stylesheet" href="ui/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="ui/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="ui/css/owl.carousel.min.css">
    <link rel="stylesheet" href="ui/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="ui/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="ui/css/aos.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="ui/css/style.css">

  </head>

  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    
    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>



      <header class="site-navbar site-navbar-target" role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">

            <div class="col-3 ">
              <div class="site-logo">
                <a href="index.php" class="font-weight-bold">Cs</a>
              </div>
            </div>

            <div class="col-9  text-right">
              

              <span class="d-inline-block d-lg-block"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>

              

              <nav class="site-navigation text-right ml-auto d-none d-lg-none" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                  <li class="active"><a href="index.php" class="nav-link">Home</a></li>
                  <li><a href="./#aboutUs" class="nav-link">About</a></li>
                  <li><a href="./#aboutUs" class="nav-link">Skills</a></li>
                  <li><a href="./#Services" class="nav-link">Services</a></li>
                  <li><a href="./#Projects" class="nav-link">Projects</a></li>
                  <li><a href="./#Blog" class="nav-link">Blog</a></li>
                  <li><a href="./#ContactUS" class="nav-link">ContactUS</a></li>
                </ul>
              </nav>
            </div>

            
          </div>
        </div>

      </header>

    <div class="ftco-blocks-cover-1">
      <div class="site-section-cover overlay" style="background-image: url('ui/images/hero_1.jpg')">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-8 text-center">
              <h1 class="mb-4 text-white">Coding Squad Software House</h1>
              <p class="mb-4">Get the Best Tech Method To Contact the World with the Best Ninja Coders</p>
              <!-- <p><a href="#" class="btn btn-primary btn-outline-white py-3 px-5">Contact Us</a></p> -->
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- ABOUT US -->
    <?php $data = mysqli_fetch_assoc(getData('aboutUS')); ?>
    <div class="site-section" id="aboutUs">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-4">
            <h2 class="h4 mb-4">About Us</h2>
            <p><?php echo $data['description']; ?></p>
            
            <!-- <p><a href="#" class="btn btn-primary text-white px-5">Our works</a></p> -->
          </div>
          <div class="col-md-4">
            <img src="CS-admin/about-us/images/<?php echo $data['image']; ?>" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-4">
            
            <h2 class="h4 mb-4">Skills</h2>
            
            <?php $sql = 'SELECT * FROM cs WHERE section = "Skills"'; $get = mysqli_query($connect,$sql);

             while($data = mysqli_fetch_assoc($get)){ 

            ?>

            <div class="progress-wrap mb-4">
              <div class="d-flex">
                <span><?php echo $data['title']; ?></span>
                <span class="ml-auto"><?php echo $data['no1']; ?>%</span>
              </div>
              <div class="progress rounded-0" style="height: 7px;">
                <div class="progress-bar" role="progressbar" style="width: <?php echo $data['no1']; ?>%;" aria-valuenow="<?php echo $data['no1']; ?>" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>
          <?php } ?>
          
          </div>
        </div>
      </div>
    </div>

    <div class="site-section bg-light" id="Services">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-7 mx-auto text-center">
            <h2 class="heading-29190">Our Services</h2>
          </div>
        </div>
        <div class="row">
          <?php $sql = 'SELECT * FROM cs WHERE section = "Services"'; $get = mysqli_query($connect,$sql);

             while($data = mysqli_fetch_assoc($get)){ 

            ?>
          <div class="col-md-6 col-lg-3">
            <div class="service-29128 text-center">
              <span class="d-block wrap-icon">
                <span class="icon-desktop_mac"></span>
              </span>
              <h3><?php echo $data['title']; ?></h3>
              <p><?php echo $data['description']; ?></p>
            </div>
          </div>
        <?php } ?>

        </div>
      </div>
    </div>

    <div class="site-section" id="Projects">
      <div class="container">

        <div class="row mb-5">
          <div class="col-md-7 mx-auto text-center">
            <h2 class="heading-29190">Our Works</h2>
          </div>
        </div>

        <div class="row">
          <?php $sql = 'SELECT * FROM cs WHERE section = "Projects"'; $get = mysqli_query($connect,$sql);

             while($data = mysqli_fetch_assoc($get)){ 

            ?>
          <div class="col-md-6 col-lg-4 mb-4">
            <div class="item web">
              <a href="<?php echo $data['link']; ?>" title="<?php echo $data['title']; ?>" class="item-wrap">
                <span class="icon-add"></span>
                <img class="img-fluid" src="CS-admin/projects/images/<?php echo $data['image']; ?>">
              </a>
            </div>
          </div>
        <?php } ?>

        </div>

        
      </div>
    </div> <!-- END .site-section -->


    <div class="site-section bg-white">
      <div class="container" style="margin-top: -100px;" id="Blog">
        <div class="row mb-5">
          <div class="col-md-7 mx-auto text-center">
            <h2 class="heading-29190">Blog</h2>
          </div>
        </div>

        <div class="row">
        
          <?php $sql = 'SELECT * FROM cs WHERE section = "Blog"'; $get = mysqli_query($connect,$sql);

             while($data = mysqli_fetch_assoc($get)){ 

            ?>
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="post-entry-1 h-100">
              <!-- <a href="single.html"> -->
                <img src="CS-admin/blog/images/<?php echo $data['image']; ?>" alt="Image"
                 class="img-fluid">
              <!-- </a> -->
              <div class="post-entry-1-contents">
                
                <h2><a href="single.html"><?php echo $data['title']; ?></a></h2>
                <span class="meta d-inline-block mb-3"><?php echo $data['modified']; ?><span class="mx-2">by</span> <a href="#">Admin</a></span>
                <p><?php echo $data['description']; ?></p>
              </div>
            </div>
          </div>
        <?php } ?>

        
      </div>
    </div>
    <?php $sql = 'SELECT * FROM cs WHERE section = "ContactUS"'; $get = mysqli_query($connect,$sql);

             $data = mysqli_fetch_assoc($get)

            ?>
    <footer class="site-footer">
     
      <div class="container" style="margin-top: -100px;" id="ContactUS">
         <div class="row mb-5">
          <div class="col-md-7 mx-auto text-center">
            <h2 class="heading-29190">Contact Us</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3">
            <h2 class="footer-heading mb-3">Address</h2>
                <p><?php echo $data['description']; ?></p>
                <h2 class="footer-heading mb-3">Email</h2>
                <p><?php echo $data['title']; ?></p>
                <h2 class="footer-heading mb-3">Phone</h2>
                <p><?php echo $data['no1']; ?> - <?php echo $data['no2']; ?></p>

                <div class="social_29128 white mb-5">
                  <a href="<?php echo $data['facebook']; ?>" title="facebook"><span class="icon-facebook"></span></a>  
                  <a href="<?php echo $data['linkedIn']; ?>" title="linkedIn"><span class="icon-linkedin"></span></a>  
                  <!-- <a href="<?php echo $data['freeLancer']; ?>" title="freeLancer"><span class="icon-freelancer"></span></a>   -->
                </div>
          </div>
          <div class="col-lg-8 ml-auto">
            <div class="row"  style="float: right;">
              <form action="#" method="post">
              <div class="form-group row">
                <div class="col-md-6 mb-4 mb-lg-0">
                  <input type="text" class="form-control" placeholder="First name">
                </div>
                <div class="col-md-6">
                  <input type="text" class="form-control" placeholder="First name">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <input type="text" class="form-control" placeholder="Email address">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <textarea name="" id="" class="form-control" placeholder="Write your message." cols="30" rows="10"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6 mr-auto">
                  <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Send Message">
                </div>
              </div>
            </form>
              
          </div>
        </div>
      </div>
    </div>
</footer>
</div>

    <script src="ui/js/jquery-3.3.1.min.js"></script>
    <script src="ui/js/jquery-migrate-3.0.0.js"></script>
    <script src="ui/js/popper.min.js"></script>
    <script src="ui/js/bootstrap.min.js"></script>
    <script src="ui/js/owl.carousel.min.js"></script>
    <script src="ui/js/jquery.sticky.js"></script>
    <script src="ui/js/jquery.waypoints.min.js"></script>
    <script src="ui/js/jquery.animateNumber.min.js"></script>
    <script src="ui/js/jquery.fancybox.min.js"></script>
    <script src="ui/js/jquery.stellar.min.js"></script>
    <script src="ui/js/jquery.easing.1.3.js"></script>
    <script src="ui/js/bootstrap-datepicker.min.js"></script>
    <script src="ui/js/isotope.pkgd.min.js"></script>
    <script src="ui/js/aos.js"></script>

    <script src="ui/js/main.js"></script>

  </body>

</html>